package com.yunfeisoft.business.dao.inter;

import com.applet.base.BaseDao;
import com.yunfeisoft.business.model.ProductCategory;
import com.applet.utils.Page;

import java.util.List;
import java.util.Map;

/**
 * ClassName: ProductCategoryDao
 * Description: 商品类别Dao
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface ProductCategoryDao extends BaseDao<ProductCategory, String> {

    public Page<ProductCategory> queryPage(Map<String, Object> params);

    public List<ProductCategory> queryList(Map<String, Object> params);

    public List<ProductCategory> queryByName(String orgId, String name);
}
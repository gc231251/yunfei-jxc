package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.yunfeisoft.business.model.IncomeRecord;
import com.applet.utils.Page;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * ClassName: IncomeRecordService
 * Description: 收款信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface IncomeRecordService extends BaseService<IncomeRecord, String> {

    public Page<IncomeRecord> queryPage(Map<String, Object> params);

    public List<IncomeRecord> queryBySaleOrderId(String saleOrderId);

    public String batchSave2(String orgId, String[] saleOrderIds, BigDecimal actualAmount);
}
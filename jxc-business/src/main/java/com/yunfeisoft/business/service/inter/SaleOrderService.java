package com.yunfeisoft.business.service.inter;

import com.applet.base.BaseService;
import com.applet.utils.Page;
import com.yunfeisoft.business.model.SaleOrder;

import java.util.Map;

/**
 * ClassName: SaleOrderService
 * Description: 销售单信息service接口
 * Author: Jackie liu
 * Date: 2020-07-23
 */
public interface SaleOrderService extends BaseService<SaleOrder, String> {

    public Page<SaleOrder> queryPage(Map<String, Object> params);

    /**
     * 改单解锁
     *
     * @param id
     * @return
     */
    public int modifyWithUnLock(String id, SaleOrder saleOrder);

    public SaleOrder queryTotalAmount(String orgId, int status, int payStatus);

    //public int modifyWithCheck(String id);
}
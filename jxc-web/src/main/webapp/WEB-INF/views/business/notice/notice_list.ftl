<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>公告信息列表</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
    <div class="app-container" @click="hideMenu">
        <div class="layui-row app-header">
            <div class="layui-col-md3">
                <button type="button" class="layui-btn layui-btn-sm" @click="add">创建公告</button>
            </div>
            <div class="layui-col-md9 text-right">
                <select v-model="params.type" class="layui-input" @change="seachData">
                    <option value="">所属栏目</option>
                    <option value="index">首页</option>
                    <option value="sale_order_add">销售开单</option>
                    <option value="sale_order_list">销售单管理</option>
                    <option value="purchase_order_add">进货开单</option>
                    <option value="purchase_order_list">进货单管理</option>
                    <option value="indec_order_add">损益开单</option>
                    <option value="indec_order_list">损益单管理</option>
                    <option value="allot_order_add">调拨开单</option>
                    <option value="allot_order_list">调拨单管理</option>
                    <option value="sale_order_chart">销售汇总</option>
                    <option value="purchase_order_chart">采购汇总</option>
                    <option value="date_chart">日期汇总</option>
                    <option value="product_stock_alarm">库存明细</option>
                    <option value="product_list">商品管理</option>
                    <option value="supplier_list">供应商管理</option>
                    <option value="customer_list">客户管理</option>
                    <option value="company_info">公司信息</option>
                    <option value="product_trace">商品追查</option>
                    <option value="user_list">账号管理</option>
                    <option value="role_list">权限管理</option>
                </select>
                <input type="text" v-model="params.title" placeholder="公告标题" class="layui-input" @keyup.13="loadData">
                <button type="button" class="layui-btn layui-btn-sm layui-btn-primary search-button" @click="seachData">查询</button>
            </div>
        </div>
        <div class="app-list">
            <div class="layui-row">
                <div class="layui-col-md9">
                    <div class="app-table-num"><span class="num">公告列表(共 {{total}} 条)</span></div>
                </div>
                <div class="layui-col-md3 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
            <table class="layui-table" lay-even lay-skin="nob" lay-size1="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>标题</th>
                    <th>所属栏目</th>
                    <th>视频</th>
                    <th>创建时间</th>
                    <th style="width:115px;">操作</th>
                </tr>
                </thead>
                <tbody>
                <tr v-for="(item, index) in rows" @dblclick="show(index)">
                    <td>{{20 * (params.page - 1) + 1 + index}}</td>
                    <td>{{item.title}}</td>
                    <td>
                        <span v-if="item.type == 'index'">首页</span>
                        <span v-if="item.type == 'sale_order_add'">销售开单</span>
                        <span v-if="item.type == 'sale_order_list'">销售单管理</span>
                        <span v-if="item.type == 'purchase_order_add'">进货开单</span>
                        <span v-if="item.type == 'purchase_order_list'">进货单管理</span>
                        <span v-if="item.type == 'indec_order_add'">损益开单</span>
                        <span v-if="item.type == 'indec_order_list'">损益单管理</span>
                        <span v-if="item.type == 'allot_order_add'">调拨开单</span>
                        <span v-if="item.type == 'allot_order_list'">调拨单管理</span>
                        <span v-if="item.type == 'sale_order_chart'">销售汇总</span>
                        <span v-if="item.type == 'purchase_order_chart'">采购汇总</span>
                        <span v-if="item.type == 'date_chart'">日期汇总</span>
                        <span v-if="item.type == 'product_stock_alarm'">库存明细</span>
                        <span v-if="item.type == 'product_list'">商品管理</span>
                        <span v-if="item.type == 'supplier_list'">供应商管理</span>
                        <span v-if="item.type == 'customer_list'">客户管理</span>
                        <span v-if="item.type == 'company_info'">公司信息</span>
                        <span v-if="item.type == 'product_trace'">商品追查</span>
                        <span v-if="item.type == 'user_list'">账号管理</span>
                        <span v-if="item.type == 'role_list'">权限管理</span>
                    </td>
                    <td>
                        <span v-if="item.videoUrl">已上传</span>
                        <span v-if="!item.videoUrl">--</span>
                    </td>
                    <td>{{item.createTimeStr}}</td>
                    <td class="more-parent">
                        <div class="ui-operating" @click="show(index)">查看</div>
                        <div class="ui-split"></div>
                        <div class="ui-operating" @click="modify(index)">编辑</div>
                        <div class="ui-split"></div>
                        <div class="ui-operating" @click.stop="showMenu(index)">更多</div>
                        <div class="more-container" v-if="item.showMenu">
                            <div class="more-item" @click="remove(index)">删除</div>
                        </div>
                    </td>
                </tr>
                <tr v-if="rows.length <= 0">
                    <td colspan="6" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
            <div class="layui-row">
                <div class="layui-col-md6">
                    <div class="app-table-num"><span class="num">共 {{total}} 条</span></div>
                </div>
                <div class="layui-col-md6 text-right">
                    <span class="prev" @click="loadPrev">上一页</span>
                    <span class="next" @click="loadNext">下一页</span>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                name:'',
                type:'',
                page: 1,
            },
            rows: [],
            total: 0,
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            seachData:function(){
                this.params.page = 1;
                this.$nextTick(function () {
                    this.loadData();
                });
            },
            loadData: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/notice/list.json", this.params).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.rows = data.rows;
                    that.total = data.total;
                });
            },
            loadNext: function () {
                if (this.rows.length < 20 && this.rows.length > 0) {
                    return;
                }
                this.params.page = this.params.page + 1;
                this.loadData();
            },
            loadPrev: function () {
                if (this.params.page <= 1) {
                    return;
                }
                this.params.page = this.params.page - 1;
                this.loadData();
            },
            add:function () {
                this.showTypes = false;
                var url = "${params.contextPath!}/view/business/notice/notice_edit.htm";
                DialogManager.open({url:url, width:'650px', height:'100%', title:'添加公告'});
            },
            modify: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/notice/notice_edit.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '编辑公告'});
            },
            show: function (index) {
                var row = this.rows[index];
                var url = "${params.contextPath!}/view/business/notice/notice_detail.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '查看公告'});
            },
            showMenu:function (index) {
                this.hideMenu();
                this.$set(this.rows[index], "showMenu", true);
            },
            hideMenu: function () {
                var that = this;
                this.rows.forEach(function (item) {
                    that.$set(item, 'showMenu', false);
                });
            },
            remove:function (index) {//删除
                var that = this;
                $.http.post("${params.contextPath}/web/notice/delete.json", {ids: this.rows[index].id}).then(function (data) {
                    $.message(data.message);
                    if (!data.success) {
                        return;
                    }
                    that.loadData();
                });
            },
        }
    });
</script>
</body>

</html>

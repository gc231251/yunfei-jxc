<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>备忘录信息详情</title>
    <#include "/common/resource.ftl">
    <script type="text/javascript">
        $(function () {
            <#if (params.id)??>
                $.ajaxRequest({
                    url: '${params.contextPath}/web/memorandum/query.json',
                    data: {id: "${params.id}"},
                    success: function (data) {
                        if (!data.success) {
                            $.message(data.message);
                            return;
                        }
                        var record = data.data;
                        for (var key in record) {
                            $("[field='" + key + "']").html(record[key]);
                        }
                    }
                });
            </#if>
        });
    </script>
</head>
<body>
<div class="ui-table-div">
    <table class="layui-table ui-table" lay-skin="nob">
        <tr>
            <td field="title" style="text-align:center;font-weight:bold;font-size:16px;">--</td>
        </tr>
        <tr>
            <td style="text-align:center;">
                <span field="beginDate">--</span> ~
                <span field="endDate">--</span>
            </td>
        </tr>
        <tr>
            <td><pre field="content"></pre></td>
        </tr>
    </table>
</div>
</body>

</html>

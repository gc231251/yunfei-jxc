<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>编辑付款信息</title>
	<#include "/common/vue_resource.ftl">
</head>
<body>
<div id="app" v-cloak>
	<div class="ui-form">
		<form class="layui-form" @submit.prevent="submitForm()" method="post">
			<div class="layui-form-item">
				<label class="layui-form-label">进货单号</label>
				<div class="layui-input-block">
					<input type="text" v-model="orderCode" class="layui-input" readonly/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">应付金额</label>
				<div class="layui-input-block">
					<input type="text" v-model="record.dueAmount" class="layui-input" readonly/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">实付金额<span class="ui-request">*</span></label>
				<div class="layui-input-block">
					<input type="text" v-model="record.payAmount" placeholder="请输入实付金额" class="layui-input"/>
				</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">备注</label>
				<div class="layui-input-block">
					<textarea v-model="record.remark" placeholder="请输入备注" class="layui-textarea"></textarea>
				</div>
			</div>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<input type="submit" value="保存" class="layui-btn" />
				</div>
			</div>
		</form>
	</div>
</div>
<script>
	var app = new Vue({
		el: '#app',
		data: {
			showTypes: false,
			record: {
				id: '${params.id!}',
				purchaseOrderId: '${params.purchaseOrderId!}',
				dueAmount: '',
				payAmount: '',
				remark: '',
			},
			orderCode: ''
		},
		mounted: function () {
			this.init();
			this.loadData();
		},
		methods: {
			init:function(){
				var that = this;
				/*laydate.render({elem: '#beginDate', type:'datetime', done:function (value) {
						that.record.beginDate = value;
					}});*/

			},
			loadData: function () {
				if (!'${params.purchaseOrderId!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/purchaseOrder/querySingle.json', {id: '${params.purchaseOrderId!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					that.orderCode = item.code;
					that.record.dueAmount = item.surplusAmount || 0;
					//that.record.payAmount = item.surplusAmount || 0;
				});
				/*if (!'${params.id!}') {
					return;
				}
				var that = this;
				$.http.post('${params.contextPath}/web/paymentRecord/query.json', {id: '${params.id!}'}).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var item = data.data;
					for (var key in  that.record) {
						that.record[key] = item[key];
					}
				});*/
			},
			submitForm: function () {
				$.http.post('${params.contextPath}/web/paymentRecord/<#if (params.id)??>modify<#else>save</#if>.json', this.record).then(function (data) {
					if (!data.success) {
						$.message(data.message);
						return;
					}
					var alt = layer.alert(data.message || "操作成功", function () {
						parent.app.loadData();
						parent.layer.closeAll();
						layer.close(alt);
					});
				});
			},
		}
	});
</script>
</body>

</html>

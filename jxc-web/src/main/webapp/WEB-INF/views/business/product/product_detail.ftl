<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>查看商品信息</title>
    <#include "/common/vue_resource.ftl">
</head>
<body style="background:#f2f2f2;padding:10px;">
<div id="app" v-cloak>
    <div class="layui-card">
        <div class="layui-card-header">基本信息</div>
        <div class="layui-card-body">
            <table class="layui-table ui-table">
                <tr>
                    <th>编码</th>
                    <td>{{record.code}}</td>
                    <th>名称</th>
                    <td>{{record.name}}</td>
                </tr>
                <tr>
                    <th>规格</th>
                    <td>{{record.standard}}</td>
                    <th>单位</th>
                    <td>{{record.unit}}</td>
                </tr>
                <tr>
                    <th>类别</th>
                    <td>{{record.categoryName}}</td>
                    <th>供应商</th>
                    <td>{{record.supplierName}}</td>
                </tr>
                <tr>
                    <@auth code='purchaseOrder_column_price'>
                        <th>进货价</th>
                        <td>￥{{record.buyPrice}}</td>
                    </@auth>
                    <@auth code='saleOrder_column_price'>
                        <th>零售价</th>
                        <td>￥{{record.salePrice}}</td>
                    </@auth>
                </tr>
                <@auth code='saleOrder_column_price'>
                    <tr>
                        <th>会员价</th>
                        <td>￥{{record.memberPrice}}</td>
                        <th>批发价</th>
                        <td>￥{{record.tradePrice}}</td>
                    </tr>
                </@auth>
                <tr>
                    <th>商品说明</th>
                    <td>{{record.intro}}</td>
                    <th>拼音简码</th>
                    <td>{{record.pinyinCode}}</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="layui-card">
        <div class="layui-card-header">库存信息</div>
        <div class="layui-card-body">
            <table class="layui-table" lay-even lay-skin="nob" lay-size="sm">
                <thead>
                <tr>
                    <th style="width:20px;">#</th>
                    <th>仓库</th>
                    <th>当前库存</th>
                    <th>缺货提醒</th>
                    <th>积压提醒</th>
                </tr>
                </thead>
                <tbody>
                <tr v-if="record.warehouseProductList" v-for="(item, index) in record.warehouseProductList">
                    <td>{{1 + index}}</td>
                    <td>{{item.warehouseName}}</td>
                    <td>{{item.stock}}</td>
                    <td>
                        {{item.shortageLimit}}/
                        <span class="ui-error" v-if="item.shortage > 0">{{item.shortage}}</span>
                        <span v-if="item.shortage <= 0">{{item.shortage}}</span>
                    </td>
                    <td>
                        {{item.backlogLimit}}/
                        <span class="ui-error" v-if="item.backlog > 0">{{item.backlog}}</span>
                        <span v-if="item.backlog <= 0">{{item.backlog}}</span>
                    </td>
                </tr>
                <tr v-if="!record.warehouseProductList || record.warehouseProductList.length <= 0">
                    <td colspan="5" class="text-center">没有更多数据了...</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            record : {},
        },
        mounted: function () {
            this.loadData();
        },
        methods: {
            loadData: function () {
                if (!'${params.id!}') {
                    return;
                }
                var that = this;
                $.http.post('${params.contextPath}/web/product/query.json', {id: '${params.id!}'}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.record = data.data;
                });
            },
        }
    });
</script>
</body>

</html>
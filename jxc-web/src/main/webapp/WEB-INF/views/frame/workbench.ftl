<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>工作台</title>
    <#include "/common/vue_resource.ftl">
    <style>
        .small-box{border-radius:2px;padding:10px;color:#fff;margin:10px;}
        .small-box h3{font-size:38px;margin:0 0 10px 0;font-weight:bold;white-space:nowrap;}
        .small-box p{font-size:15px;}
        .layui-card-header {font-size: 18px;}
        .box{height:300px;overflow:auto;}
        .search {float: right;padding-left: 5px;}
        /*body{background-image: url(${params.contextPath!}/adminui/ui/img/login-background.jpg);background-repeat:no-repeat;background-size:cover;}
        .bg1{background-color:rgba(255,255,255,0.4) !important;color:#fff;}*/
    </style>
</head>
<body>
<div id="app" v-cloak>
    <div style="padding:10px;">
        <#--今日数据统计-->
        <div class="layui-row bg" style="padding:10px;background:#fff;margin-bottom:10px;">
            <div class="layui-col-xs3 layui-col-sm3 layui-col-md3">
                <div class="small-box" style="background:#00c0ef;">
                    <h3>{{saleNum}}单</h3>
                    <p>今日销售单数</p>
                </div>
            </div>
            <div class="layui-col-xs3 layui-col-sm3 layui-col-md3">
                <div class="small-box" style="background:#00a65a;">
                    <h3>￥{{saleAmount}}</h3>
                    <p>今日销售总额</p>
                </div>
            </div>
            <div class="layui-col-xs3 layui-col-sm3 layui-col-md3">
                <div class="small-box" style="background:#f39c12;">
                    <h3>{{purchaseNum}}单</h3>
                    <p>今日采购单数</p>
                </div>
            </div>
            <div class="layui-col-xs3 layui-col-sm3 layui-col-md3">
                <div class="small-box" style="background:#dd4b39;">
                    <h3>￥{{purchaseAmount}}</h3>
                    <p>今日采购总额</p>
                </div>
            </div>
        </div>

        <div class="layui-row layui-col-space10">
            <div class="layui-col-md6">
                <div class="layui-card bg">
                    <div class="layui-card-header">公告
                        <div class="search">
                            <button type="button" class="layui-btn layui-btn-normal layui-btn-xs" style="margin-top:-5px;" @click="showNoticeMore">更多</button>
                        </div>
                    </div>
                    <div class="layui-card-body box">
                        <table class="layui-table" lay-skin="nob" lay-size1="sm">
                            <tr v-for="(item, index) in noticeList">
                                <td>{{item.title}}</td>
                                <#--<td>{{item.createTimeStr}}</td>-->
                                <td class="more-parent">
                                    <div class="ui-operating" @click="showNotice(index)">查看</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="layui-col-md6">
                <div class="layui-card bg">
                    <div class="layui-card-header">备忘录
                        <div class="search">
                            <button type="button" class="layui-btn layui-btn-xs" style="margin-top:-5px;" @click="addMemorandum">添加</button>
                            <button type="button" class="layui-btn layui-btn-normal layui-btn-xs" style="margin-top:-5px;" @click="showMemorandumMore">更多</button>
                        </div>
                    </div>
                    <div class="layui-card-body box">
                        <table class="layui-table" lay-skin="nob" lay-size1="sm">
                            <tr v-for="(item, index) in memorandumList">
                                <td>{{item.title}}</td>
                                <td>{{item.beginDate}}</td>
                                <td>{{item.endDate}}</td>
                                <td class="more-parent" style="width:115px;">
                                    <div class="ui-operating" @click="showMemorandum(index)">查看</div>
                                    <div class="ui-split"></div>
                                    <div class="ui-operating" @click="modifyMemorandum(index)">编辑</div>
                                    <div class="ui-split"></div>
                                    <div class="ui-operating" @click="removeMemorandum(index)">删除</div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="layui-card bg" style="margin-top:10px;">
            <div class="layui-card-header">其他</div>
            <div class="layui-card-body">
                <div class="layui-row layui-col-space10">
                    <div class="layui-col-md6 text-center">
                        <div><img src="${params.contextPath}/images/miniapp.jpg" alt="" style="height:200px;"></div>
                        <div style="padding:10px 0;">微信扫码登录小程序</div>
                    </div>
                    <div class="layui-col-md6 text-center">
                        <div><img src="${params.contextPath}/images/kefu.png" alt="" style="height:200px;"></div>
                        <div style="padding:10px 0;">微信扫码咨询客服</div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            params: {
                saleBeginDate: '',
                saleEndDate: '',
                purchaseBeginDate: '',
                purchaseEndDate: '',
                purchaseType: 'week',
                saleType: 'week',
            },
            saleNum: '0',
            saleAmount: '0',
            purchaseNum: '0',
            purchaseAmount: '0',
            noticeList: [],
            memorandumList: [],
        },
        mounted: function () {
            this.loadNow();

            var that = this;
            laydate.render({
                elem: '#beginDate', type: 'date', done: function (value) {
                    that.params.beginDate = value;
                }
            });

            laydate.render({
                elem: '#endDate', type: 'date', done: function (value) {
                    that.params.endDate = value;
                }
            });
        },
        methods: {
            loadNow: function () {
                var that = this;
                that.loadNotice();
            },
            loadNotice: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/notice/list.json", {
                    page: 1,
                    rows: 10,
                    type: 'index'
                }).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.noticeList = data.rows;
                    that.loadMemorandum();
                });
            },
            loadMemorandum: function () {
                var that = this;
                $.http.post("${params.contextPath}/web/memorandum/list.json", {page: 1, rows: 10}).then(function (data) {
                    if (!data.success) {
                        $.message(data.message);
                        return;
                    }
                    that.memorandumList = data.rows;
                });
            },
            showNotice:function (index) {
                var row = this.noticeList[index];
                var url = "${params.contextPath!}/view/business/notice/notice_detail.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '查看公告'});
            },
            showNoticeMore:function () {
                var url = "${params.contextPath!}/view/business/notice/notices.htm";
                DialogManager.open({url: url, width: '80%', height: '100%', title: '公告列表'});
            },
            addMemorandum:function () {
                var url = "${params.contextPath!}/view/business/memorandum/memorandum_edit.htm";
                DialogManager.open({url: url, width: '650px', height: '100%', title: '备忘录列表'});
            },
            showMemorandumMore:function () {
                var url = "${params.contextPath!}/view/business/memorandum/memorandum_list.htm";
                DialogManager.open({url: url, width: '80%', height: '100%', title: '备忘录列表'});
            },
            showMemorandum:function (index) {
                var row = this.memorandumList[index];
                var url = "${params.contextPath!}/view/business/memorandum/memorandum_detail.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '查看公告'});
            },
            modifyMemorandum: function (index) {
                var row = this.memorandumList[index];
                var url = "${params.contextPath!}/view/business/memorandum/memorandum_edit.htm?id=" + row.id;
                DialogManager.open({url: url, width: '650px', height: '100%', title: '编辑备忘录'});
            },
            removeMemorandum:function (index) {//删除
                var that = this;
                layer.confirm('确定删除记录？', function () {
                    $.http.post("${params.contextPath}/web/memorandum/delete.json", {ids: that.memorandumList[index].id}).then(function (data) {
                        $.message(data.message);
                        if (!data.success) {
                            return;
                        }
                        that.loadMemorandum();
                    });
                });
            },
        }
    });
</script>
</body>

</html>

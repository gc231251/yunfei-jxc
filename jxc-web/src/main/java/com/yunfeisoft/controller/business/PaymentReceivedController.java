package com.yunfeisoft.controller.business;

import com.applet.base.BaseController;
import com.applet.utils.Page;
import com.applet.utils.Response;
import com.applet.utils.ResponseUtils;
import com.applet.utils.Validator;
import com.yunfeisoft.business.model.PaymentReceived;
import com.yunfeisoft.business.service.inter.PaymentReceivedService;
import com.yunfeisoft.model.User;
import com.yunfeisoft.utils.ApiUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * ClassName: PaymentReceivedController
 * Description: 收付款信息Controller
 * Author: Jackie liu
 * Date: 2020-08-11
 */
@Controller
public class PaymentReceivedController extends BaseController {

    @Autowired
    private PaymentReceivedService paymentReceivedService;

    /**
     * 添加收付款信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/paymentReceived/save", method = RequestMethod.POST)
    @ResponseBody
    public Response save(PaymentReceived record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "occurDate", "日期为空");
        validator.required(request, "type", "类型为空");
        validator.required(request, "name", "项目为空");
        validator.required(request, "amount", "金额为空");
        validator.number(request, "amount", "金额不合法");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }

        User user = ApiUtils.getLoginUser();
        record.setOrgId(user.getOrgId());

        paymentReceivedService.save(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 修改收付款信息
     *
     * @param record
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/paymentReceived/modify", method = RequestMethod.POST)
    @ResponseBody
    public Response modify(PaymentReceived record, HttpServletRequest request, HttpServletResponse response) {
        Validator validator = new Validator();
        validator.required(request, "id", "参数错误");
        validator.required(request, "occurDate", "日期为空");
        validator.required(request, "type", "类型为空");
        validator.required(request, "name", "项目为空");
        validator.required(request, "amount", "金额为空");
        validator.number(request, "amount", "金额不合法");
        if (validator.isError()) {
            return ResponseUtils.warn(validator.getMessage());
        }
        paymentReceivedService.modify(record);
        return ResponseUtils.success("保存成功");
    }

    /**
     * 查询收付款信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/paymentReceived/query", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response query(HttpServletRequest request, HttpServletResponse response) {
        String id = ServletRequestUtils.getStringParameter(request, "id", null);
        if (StringUtils.isBlank(id)) {
            return ResponseUtils.warn("参数错误");
        }
        PaymentReceived record = paymentReceivedService.load(id);
        return ResponseUtils.success(record);
    }

    /**
     * 分页查询收付款信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/paymentReceived/list", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response list(HttpServletRequest request, HttpServletResponse response) {
        String name = ServletRequestUtils.getStringParameter(request, "name", null);

        User user = ApiUtils.getLoginUser();

        Map<String, Object> params = new HashMap<String, Object>();
        initParams(params, request);
        params.put("name", name);
        params.put("orgId", user.getOrgId());

        Page<PaymentReceived> page = paymentReceivedService.queryPage(params);
        return ResponseUtils.success(page);
    }

    /**
     * 批量删除收付款信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/web/paymentReceived/delete", method = {RequestMethod.POST, RequestMethod.GET})
    @ResponseBody
    public Response delete(HttpServletRequest request, HttpServletResponse response) {
        String ids = ServletRequestUtils.getStringParameter(request, "ids", null);
        if (StringUtils.isBlank(ids)) {
            return ResponseUtils.warn("参数错误");
        }
        paymentReceivedService.remove(ids.split(","));
        return ResponseUtils.success("删除成功");
    }
}

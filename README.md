# 云飞进销存系统

## 其他系统

```
云飞商城：https://gitee.com/jackieliu789/yunfei-mall
```
```
云飞机构养老管理平台：https://gitee.com/jackieliu789/yunfei-yns
```
```
云飞在线互动课堂：https://gitee.com/jackieliu789/yunfei-course
```

#### 介绍
【微商版】进销存系统，支持微信小程序端、电脑端、支持商品扫码、订单商品扫码等

```
1：功能简洁易懂，不懂财务也能轻松上手
2：手机，平板，电脑数据实时同步
3：多账户权限管理，老板一键屏蔽系统进货价格，销售价格。
4：支持多仓库，多门店
5：一键分享进货单，销售单，库存商品
6：手机扫码出库，进库，无需另外购买扫码设备
7：全国各行各业电商大佬实测功能，真正实现外出 就能轻松办公
```

【企业版】进销存系统，支持小程序端、电脑端、APP、H5

```
1 支持销售单、销售出库单、销售退货单分离
2 支持采购单、采购入库单、采购退货单分离
3 支持其他采购单、其他销售单
4 支持开启或者关闭单据审核功能
5 支持审核流程自定义
6 支持不同仓库之间调拨商品
7 企业账户管理
8 支持收款单、付款单、资金转账单管理
9 支持其他收入单、其他支出单管理
10 支持商品多规格、按照客户或批发数量设置销售价、按照供应商或者批发数量设置进货价
11 支持开启或关闭负库存
12 支持开启或者关闭非编辑状态下的单据备注也能修改
13 支持开启或者关闭任意状态下可上传附件
14 支持开启或者关闭单据税率
15 支持自定义打印单据样式
16 支持主要模块设置自定义字段
17 支持自定义单据编号规则
18 移动端支持H5、小程序、APP
19 支持替换公司的信息包括：logo、系统名称、移动端菜单图标
20 私有化部署
21 支持微信小程序端商城、在线微信支付
22 支持三级分销
```

#### （微商版）软件演示地址

1.  官网地址：http://yunfeisoft.com
2.  演示地址：https://jxc.yunfeisoft.com:558/login
3.  账号：0001   密码：123456a
4.  QQ群：1148468582
5.  小程序二维码：<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/jxc_qrcode.jpg" alt="云飞进销存" style="zoom:25%;" width="20%"/>

#### （企业版）软件演示地址

1.  官网地址：http://yunfeisoft.com
2.  演示地址：https://qyjxc.yunfeisoft.com
3.  账号：test  密码：123456a
4.  QQ群：1148468582
5.  Android端下载：<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/android.png" alt="云飞进销存" style="zoom:25%;" width="20%"/>
6.  小程序二维码：<img src="https://qyjxc.yunfeisoft.com/static/img/qrcode.png" alt="云飞进销存" style="zoom:25%;" width="20%"/>

#### 联系作者

1.  **如需商业版小程序端和完整系统请添加作者微信**（商业版分为：单商户版、SaaS多商户平台版、源码版）
2.  商业版移动端端和完整系统会不定期更新功能和修复已知的BUG
3.  作者微信（添加时请备注“进销存”）：<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/kefu.png" alt="云飞进销存" width="25%"/>

#### 【微商版】升级日志

```
1.4.
  a）移动端、PC管理端全面更新UI，新的UI更清爽漂亮实用
  b）开发框架全面转向SpringBoot，Vue
  c）商品支持上传多张图片
  d）进货单、销售单支持上传多张图片
  e）全新的打印自定义设计
  f）开启或关闭任意状态下修改单据备注
  g）开启或关闭任意状态下可上传附件或删除附件
  h）支持客户、供应商、商品、进货单、收货单自定义字段，系统默认字段满足不了的可以自定义了
  i）支持自定义单据、供应商、客户等信息的编号格式

1.3.
  a）修复开源版中的BUG
  
1.2. 
  a）产品资料增加产地信息
  b）产品类别选择页面增加【添加】按钮
  c）销售单及进货单管理增加自定义字段，额外新增仓库和产地
  d）日期汇总，添加一份按商品分析，饼状图分析，不同颜色显示不同的产品
  e）支持销售单和采购单打印页面中添加公共备注、客户签署栏、制单员栏
  f) 销售单及进货单增加查询条件：编码，产地，仓库，客户，数量：关键词  查询订单，关联查询

1.1. 
  a）PC端支持桌面软件打开系统，去掉浏览器标题栏、状态栏、地址栏等等占用屏幕空间的内容，还你一个干净整洁的系统
  b）支持销售单和采购单上传图片附件，方便你和你的客户进行对账
  c）订单中的商品列表项支持排序，让新添加的订单商品不再乱序
  d）新增我的销售单、我的采购单两个菜单列表项，这里只能看自己创建的订单
  e）支持销售单和采购单打印页面中添加公司经营范围
```

#### 【企业版】升级日志

```
2.1. 
  a）盘点单开发完成

2.2. 
  a）盘点单支持移动端
  b）出库单、入库单、销售单、采购单、盘点单支持扫码添加商品

2.3.
  a) 支持商城下单、在线微信支付、在线提现到支付宝或微信零钱
  b) 支持微信小程序端商品分销
  c) 支持三级分销

```

#### 【企业版】软件截图

![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/qy1.png)
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/qy2.jpg" alt="云飞进销存系统" width="30%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/qy3.jpg" alt="云飞进销存软件" width="30%"  align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/qy4.jpg" alt="云飞进销存系统" width="30%" align="left"/>






























#### 【微商版】软件截图


![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/date_chart.png)
![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/web_1.png)
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_1.jpg" alt="云飞进销存系统" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_2.jpg" alt="云飞进销存软件" width="33%"  align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_3.jpg" alt="云飞进销存软件" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_4.jpg" alt="云飞进销存软件" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_5.jpg" alt="云飞进销存软件" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_6.jpg" alt="云飞进销存软件" width="33%" align="left"/>
<img src="https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/miniapp_7.jpg" alt="云飞进销存软件" width="33%" align="left" />

![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/modify_order.png)
![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/order.png)
![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/print.png)
![云飞进销存软件](https://gitee.com/jackieliu789/yunfei-jxc/raw/master/image/trace.png)

